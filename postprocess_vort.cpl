! This program computes the means and variances
! of the vorticity field components in HST. The vorticity
! field is denoted as Omega and its components are:
!   - streamwise :   xi
!   - spanwise   :  eta
!   - vertical   : zeta

WRITE ""
WRITE ""
WRITE "-------------------------------------- VORTICITY --------------------------------------"
WRITE ""
WRITE ""

!USE rtchecks
USE scddnsdata
USE convenience

! Vorticity components, mean and rms initialization
VORTICITY=STRUCTURE(COMPLEX xi,eta,zeta)
ARRAY(0..nx,-ny..ny,-1..nz+1) OF VORTICITY Omega=0
REALVOR=STRUCTURE(REAL xim,etam,zetam)
SPECTRUMVOR=STRUCTURE(REAL xixi,xieta,xizeta,etaeta,etazeta,zetazeta)
REALVOR vort_mean(0..nz)=0; SPECTRUMVOR vort_rms(0..nz)=0
! Velocity derivatives initialization
ARRAY(nxA(myAproc)..nxA(myAproc+1)-1,nyB(myBproc)..nyB(myBproc+1)-1,-1..nz+1) OF DERIVS Vder=0

! To compute the vorticity components we need the
! velocity derivatives in the vertical direction.
! In HST we have to take into account the shear-periodic b.c.s
! The function to compute correctly derivatives in the vertical
! direction (with shear-periodic b.c.) are defined in "scddnsdata"
! and "convenience".

WRITE ""
WRITE "POSTPROCESS PARAMETERS:"

! INPUTS 
! -----------------------------
INTEGER nfmin,nfmax,nftot,dn,nf
ASK nfmin, nfmax, dn
nftot=[(nfmax-nfmin) DIV dn]+1


WRITE " "
WRITE "COMPUTING VORTICITY VARIANCES..."

! Compute vorticity variances
! ------------------------------------
LOOP phases FOR k_phase=1 TO dn
    WRITE "PHASE: "k_phase
    nf=0; nftot=0
    vort_mean=0; vort_rms=0

    LOOP fields FOR n=nfmin+k_phase-1 TO nfmax BY dn

        Omega=0;
        ! Read velocity field
        STRING field_name = WRITE('field'n'.fld'); read_field(field_name)
        ! Setup the derivatives at the correct time
        setup_derivatives()
        ! Compute derivatives
        velocity_gradient(Vder)
        ! Update periodic boundary condition for Vder
        DO pbc(Vder(ix,iy,*,i)) FOR ALL ix,iy,i
        ! Define vorticity components
        WITH Vder(*,*,*),Omega(*,*,*):
            xi=wy-vz; eta=uz-wx; zeta=vx-uy
        ! Compute mean vorticity components
        LOOP FOR iz=vort_mean.LO TO vort_mean.HI WITH vort_mean(iz)
            xim   =~+Omega(0,0,iz).xi.REAL
            etam  =~+Omega(0,0,iz).eta.REAL
            zetam =~+Omega(0,0,iz).zeta.REAL
        REPEAT
        ! Compute vorticity variances
        LOOP FOR iz=vort_rms.LO TO vort_rms.HI WITH vort_rms(iz)
            xixi     = ~ + [NORM[Omega(0,*,iz).xi] + 2*[SUM NORM[Omega(ix,*,iz).xi] FOR ix=1 TO nx]] 
            etaeta   = ~ + [NORM[Omega(0,*,iz).eta] + 2*[SUM NORM[Omega(ix,*,iz).eta] FOR ix=1 TO nx]]  
            zetazeta = ~ + [NORM[Omega(0,*,iz).zeta] + 2*[SUM NORM[Omega(ix,*,iz).zeta] FOR ix=1 TO nx]]
            xieta    = ~ + [2*[SUM (Omega(ix,*,iz).xi | Omega(ix,*,iz).eta).REAL FOR ix=1 TO nx]]
            xieta    = ~ + [Omega(0,*,iz).xi | Omega(0,*,iz).eta].REAL
            xizeta   = ~ + [2*[SUM (Omega(ix,*,iz).xi | Omega(ix,*,iz).zeta).REAL FOR ix=1 TO nx]]
            xizeta   = ~ + [Omega(0,*,iz).xi | Omega(0,*,iz).zeta].REAL
            etazeta  = ~ + [2*[SUM (Omega(ix,*,iz).eta | Omega(ix,*,iz).zeta).REAL FOR ix=1 TO nx]]
            etazeta  = ~ + [Omega(0,*,iz).eta | Omega(0,*,iz).zeta].REAL
        REPEAT LOOP
    ! Compute number of fields for each phase
    nf = ~ + 1
    REPEAT fields

    nftot=nf

    DO WITH vort_mean(iz):
        xim=~/nftot; etam=~/nftot; zetam=~/nftot
    FOR ALL iz

    DO WITH vort_rms(iz),vort_mean(iz):
        xixi=SQRT{xixi/nftot-xim^2}; etaeta=SQRT{etaeta/nftot-etam^2}; zetazeta=SQRT{zetazeta/nftot-zetam^2}
        xieta=xieta/nftot-xim*etam; xizeta=xizeta/nftot-xim*zetam; etazeta=etazeta/nftot-etam*zetam 
    FOR ALL iz

    ! Write output files
    ! -----------------------------------
    FILE out=CREATE("vort_mean_"k_phase".dat")
    WRITE TO out "# z	xi	eta zeta"
    DO WITH vort_mean(iz):
            WRITE TO out z(iz),xim,etam,zetam FOR iz=0 TO nz
    CLOSE out

    FILE out=CREATE("vort_rms_"k_phase".dat")
    WRITE TO out "# z	xi'	eta'	zeta'	xieta	xizeta	etazeta"
    DO WITH vort_rms(iz): 
            WRITE TO out z(iz),xixi,etaeta,zetazeta,xieta,xizeta,etazeta FOR iz=0 TO nz
    CLOSE out

REPEAT phases

STOP