! This program computes the MKE and TKE budgets
! and their volume integrals for the homogenenous
! shear turbulence

WRITE ""
WRITE ""
WRITE "------------------------------ REYNOLDS STRESSES BUDGET -------------------------------"
WRITE ""
WRITE ""
!
! mean TKE buget according to equation:
! 
! INST    --> dK/dt
! CONV    --> Ui*dK/dxi
! PROD    --> -<uiuj>dUj/dxi
! DISS*   --> nu<(duj/dxi + dui/dxj)*duj/dxi>
! TDIFF   --> -0.5*d/dxi<ui*uj*uj>
! PDIFF   --> -d/dxi<ui*p>
! VDIFF1  --> nu*d2K/dxi2
! VDIFF2* --> nu*d2/dxjdxi<ui*uj>
! 
! *-terms can be summed into the PDISS=nu*<duj/dxi*duj/dxi>
! 
! which in a statistically stationary and fully-developed 
! homogeneous shear turbulence flow reduces to
!
! PROD  --> -<uv>dU/dy-<vw>dW/dy         [this is computed after the fields loop]
! PDISS --> nu*<dui/dxj*dui/dxj>
! TDIFF --> -0.5*d/dy(<vuu>+<vvv>+<vww>)
! PDIFF --> -d/dy<vp>
! VDIFF --> nu*d2K/dy2                   [this is computed after the fields loop]
! 

! 
! MKE buget equation, which in a statistically stationary
! and fully-developed turbulent channel flow
! with spanwise wall oscillations reduces to
! 
! pump  --> -dP/dx*U = tau_w*U
! TPROD  --> <uv>dU/dy+<vw>dW/dy         [TKE production, here a sink]
! ttrsp  --> -d(<uv>U)/dy-d(<vw>W)/dy
! vdiff  --> nu*d(U*Uz)/dy+d(W*Vz)/dy
! dissU  --> nu*dUdy^2
! dissW  --> nu*dWdy^2
!
! all these terms are computed after the fields loop.

! This code is NOT parallel

!USE rtchecks
USE scddnsdata
USE convenience

WRITE ""
WRITE "POSTPROCESS PARAMETERS:"
! INPUTS 
! -----------------------------
INTEGER nfmin,nfmax,nftot,dn
ASK nfmin, nfmax, dn
nftot=[(nfmax-nfmin) DIV dn]+1

! Definitions
! ------------------------------------
#define cprod(f,g,h) h.REAL=f.REAL*g.REAL; h.IMAG=f.IMAG*g.IMAG
#define cprod3(f,g,k,h) h.REAL=f.REAL*g.REAL*k.REAL; h.IMAG=f.IMAG*g.IMAG*k.IMAG
MEANTERMS = STRUCTURED ARRAY(U,Vm,W,Uz,Vz,Uzz,Vzz,P) OF REAL
MKETERMS = STRUCTURED ARRAY(pump,produw,prodvw,ttrsp,vdiff,dissU,dissV,PHIttrsp,PHIvdiff) OF REAL 
BALANCE = STRUCTURED ARRAY(var,prod,psdiss,ttrsp,vdiff,pstrain,ptrsp,PHIttrsp,PHIvdiff,PHIptrsp) OF ARRAY(1..6) OF REAL

! Variables in memory
! ------------------------------------
ARRAY(nxA(myAproc)..nxA(myAproc+1)-1,nyB(myBproc)..nyB(myBproc+1)-1,-1..nz+1) OF DERIVS Vder=0
ARRAY(nxA(myAproc)..nxA(myAproc+1)-1,nyB(myBproc)..nyB(myBproc+1)-1,-1..nz+1) OF COMPLEX pressure=0
ARRAY(0..nxd-1,0..nyd-1) OF DERIVS Vderd=0
ARRAY(0..nxd-1,0..nyd-1) OF DERPRODSEXT Vmderd=0
ARRAY(0..nxd-1,0..nyd-1,1..6) OF COMPLEX PHIttrspd=0

ARRAY(-1..nz+1) OF MEANTERMS mean=0
ARRAY(-1..nz+1) OF BALANCE uiuj=0
ARRAY(-1..nz+1) OF MKETERMS mke=0

! Variables on disk
! -------------------------------------
POINTER TO STORED ARRAY(0..nx,-ny..ny,-1..nz+1) OF COMPLEX pressuredata

! Compute the average field (required for Reynolds decomposition)
! ------------------------------------
WRITE " "
WRITE "COMPUTING AVERAGE FIELD..."
LOOP files FOR n=nfmin TO nfmax BY dn
  STRING field_name = WRITE('test_nwo/field'n'.fld')
  STRING pressure_name = WRITE('test_nwo/pField'n'.fld')
  read_field(field_name); pressuredata=OPEN(pressure_name)
  WITH V(0,0,*),mean(*):
    U=~+u.REAL; Vm=~+v.REAL; W=~+w.REAL
    P=~+pressuredata(0,0,*).REAL
  CLOSE(pressuredata)
REPEAT files
setup_derivatives() ! It is not important at which time we set the derivatives
                    ! since we derive the mean
updateD0mat(0)
WITH mean: U=~/nftot; Vm=~/nftot; W=~/nftot; P=~/nftot
WITH mean(*): derivm(U, Uz);    derivm(Vm, Vz);  
WITH mean(*): deriv2m(U, Uzz);  deriv2m(Vm, Vzz);
! These means do not include the mean shear (i.e., U=Sz).
! These terms must be considered to properly compute the production terms.
! The artificial Stokes layer (i.e., V=V_SL) SHOULD  be included because we impose it by explicitly writing V(0,0,iz)=V_SL(iz).
! To include the mean shear S, we can write Uz = Uz + S once and for all
DO WITH mean(iz): Uz=~+S FOR ALL iz
     
! Compute the TKE budget
! ------------------------------------
WRITE " "
WRITE "COMPUTING TKE BUDGET..."
LOOP fields FOR n=nfmin TO nfmax BY dn
  ! Read field
  STRING field_name = WRITE('test_nwo/field'n'.fld')
  read_field(field_name)
  ! Pressure field
  STRING pressure_name = WRITE('test_nwo/pField'n'.fld'); pressuredata=OPEN(pressure_name)
  pressure=pressuredata; CLOSE(pressuredata)
  ! Compute fluctuating field
  WITH mean(*),V(0,0,*),Vder(0,0,*): u.REAL=~-U; v.REAL=~-Vm; pressure(0,0,*)=~-P;
  ! Setup the derivatives at the correct time
  setup_derivatives()
  ! Compute derivatives
  velocity_gradient(Vder)
  ! Update periodic boundary condition for Vder
  DO pbc(Vder(ix,iy,*,i)) FOR ALL ix,iy,i
  ! Parseval theorem method for var and pstrain
  LOOP FOR iz=1 TO nz-1 WITH uiuj(iz):
    LOOP FOR ALL ix,iy WITH V(ix,iy,iz),Vder(ix,iy,iz):
      p == pressure(ix,iy,iz)
      c=IF ix=0 THEN 1 ELSE 2
      var(1) = ~ + c*(u|u).REAL; var(2) = ~ + c*(v|v).REAL; var(3) = ~ + c*(w|w).REAL
      var(4) = ~ + c*(u|v).REAL; var(5) = ~ + c*(u|w).REAL; var(6) = ~ + c*(v|w).REAL

      ! PHIptrsp(2) = ~ - 2*c*(v|p).REAL; PHIptrsp(4) = ~ - c*(u|p).REAL;  PHIptrsp(6) = ~ - c*(w|p).REAL
      PHIptrsp(1) = 0; PHIptrsp(2) = 0;                PHIptrsp(3) = ~ - 2*c*(w|p).REAL
      PHIptrsp(4) = 0; PHIptrsp(5) = ~ - c*(u|p).REAL; PHIptrsp(6) = ~ - c*(v|p).REAL

      pstrain(1) = ~ + 2*c*[ux|p].REAL; pstrain(4) = ~ + c*[(uy|p)+(vx|p)].REAL
      pstrain(2) = ~ + 2*c*[vy|p].REAL; pstrain(5) = ~ + c*[(uz|p)+(wx|p)].REAL
      pstrain(3) = ~ + 2*c*[wz|p].REAL; pstrain(6) = ~ + c*[(vz|p)+(wy|p)].REAL
    REPEAT
  REPEAT
  ! Pseudo-spectral method for psdiss and PHIttrsp
  LOOP FOR iz=-1 TO nz+1

    LOOP FOR ix=0 TO nx

      DO Vderd(ix,iy)=Vder(ix,iy,iz) FOR iy=0 TO ny
      DO Vderd(ix,iy)=0 FOR iy=ny+1 TO nyd-ny-1
      DO Vderd(ix,nyd+iy)=Vder(ix,iy,iz) FOR iy=-ny TO -1

      DO Vd(ix,iy)=V(ix,iy,iz) FOR iy=0 TO ny
      DO Vd(ix,iy)=0 FOR iy=ny+1 TO nyd-ny-1
      DO Vd(ix,nyd+iy)=V(ix,iy,iz) FOR iy=-ny TO -1

      WITH Vderd(ix,*): INLINE LOOP FOR ii IN (ux,vx,wx,uy,vy,wy,uz,vz,wz); IFT(ii); REPEAT
      WITH Vd(ix,*):    INLINE LOOP FOR i  IN (u,v,w); IFT(i); REPEAT
    REPEAT LOOP

    DO Vd(ix)=0 FOR ix=nx+1 TO nxd-1
    DO Vderd(ix)=0 FOR ix=nx+1 TO nxd-1

    DO
      WITH Vderd(*,iy): INLINE LOOP FOR ii IN (ux,vx,wx,uy,vy,wy,uz,vz,wz); RFT(ii); REPEAT
      WITH Vd(*,iy):    INLINE LOOP FOR i  IN (u,v,w); RFT(i); REPEAT
      DO WITH Vderd(ix,iy), Vmderd(ix,iy), Vd(ix,iy):
          cprod(ux,ux,ux2);  cprod(uy,uy,uy2);  cprod(uz,uz,uz2);  cprod(vx,vx,vx2);  cprod(vy,vy,vy2);  cprod(vz,vz,vz2)
          cprod(wx,wx,wx2);  cprod(wy,wy,wy2);  cprod(wz,wz,wz2);  cprod(ux,vx,uxvx); cprod(uy,vy,uyvy); cprod(uz,vz,uzvz)
          cprod(ux,wx,uxwx); cprod(uy,wy,uywy); cprod(uz,wz,uzwz); cprod(vx,wx,vxwx); cprod(vy,wy,vywy); cprod(vz,wz,vzwz)
          cprod3(u,u,w,PHIttrspd(ix,iy,1));   cprod3(v,v,w,PHIttrspd(ix,iy,2));    cprod3(w,w,w,PHIttrspd(ix,iy,3))
          cprod3(u,v,w,PHIttrspd(ix,iy,4));   cprod3(u,w,w,PHIttrspd(ix,iy,5));    cprod3(v,w,w,PHIttrspd(ix,iy,6));
      FOR ALL ix
      WITH Vmderd(*,iy): INLINE LOOP FOR iii IN (ux2,vx2,wx2,uy2,vy2,wy2,uz2,vz2,wz2,uxvx,uyvy,uzvz,uxwx,uywy,uzwz,vxwx,vywy,vzwz); HFT(iii); REPEAT
      DO HFT(PHIttrspd(*,iy,i)) FOR ALL i
    FOR iy=0 TO nyd-1

    DO WITH Vmderd(ix,*):
      INLINE LOOP FOR iii IN (ux2,vx2,wx2,uy2,vy2,wy2,uz2,vz2,wz2,uxvx,uyvy,uzvz,uxwx,uywy,uzwz,vxwx,vywy,vzwz); FFT(iii); REPEAT
      DO FFT(PHIttrspd(ix,*,i)); FOR ALL i
    FOR ix=0 TO nx+1

    WITH Vmderd(0,0), uiuj(iz):
      psdiss(1)=~+2*nu*REAL[ux2+uy2+uz2]; psdiss(4)=~+2*nu*REAL[uxvx+uyvy+uzvz]
      psdiss(2)=~+2*nu*REAL[vx2+vy2+vz2]; psdiss(5)=~+2*nu*REAL[uxwx+uywy+uzwz]
      psdiss(3)=~+2*nu*REAL[wx2+wy2+wz2]; psdiss(6)=~+2*nu*REAL[vxwx+vywy+vzwz]

      PHIttrsp(1)=~-PHIttrspd(0,0,1).REAL; PHIttrsp(4)=~-PHIttrspd(0,0,4).REAL
      PHIttrsp(2)=~-PHIttrspd(0,0,2).REAL; PHIttrsp(5)=~-PHIttrspd(0,0,5).REAL
      PHIttrsp(3)=~-PHIttrspd(0,0,3).REAL; PHIttrsp(6)=~-PHIttrspd(0,0,6).REAL
      
  REPEAT 
REPEAT fields
updateD0mat(0)
! var,PHIttrsp,psdiss,pstrain
DO WITH uiuj(iz): var(i)=~/nftot; PHIptrsp(i)=~/nftot; PHIttrsp(i)=~/nftot; psdiss(i)=~/nftot; pstrain(i)=~/nftot; FOR i=1 TO 6 AND ALL iz
! PHIvdiff, vdiff
DO WITH uiuj: derivm(var(*,i),PHIvdiff(*,i)); PHIvdiff(*,i)=~*nu; FOR i=1 TO 6
DO WITH uiuj: deriv2m(var(*,i),vdiff(*,i));   vdiff(*,i)=~*nu;    FOR i=1 TO 6
! ttrsp
DO WITH uiuj: derivm(PHIttrsp(*,i),ttrsp(*,i)); FOR i=1 TO 6
! ptrsp
DO WITH uiuj: derivm(PHIptrsp(*,i),ptrsp(*,i)); FOR i=1 TO 6
! prod
DO WITH mean(iz), uiuj(iz):
  prod(1)=-2*var(5)*Uz;         prod(2)=-2*var(6)*Vz; prod(3)=0
  prod(4)=-var(5)*Vz-var(6)*Uz; prod(5)=-var(3)*Uz;   prod(6)=-var(3)*Vz
FOR ALL iz
DO WITH mean(iz), uiuj(iz), mke(iz): produw=-0.5*prod(1); prodvw=-0.5*prod(2) FOR ALL iz
! dissU(MKE) and dissW(MKE)
DO WITH mean(iz), mke(iz): dissU=nu*Uz^2; dissV=nu*Vz^2 FOR ALL iz
! ttrsp(MKE)
DO WITH mean(iz), uiuj(iz), mke(iz): mke(iz).PHIttrsp=-var(5)*U-var(6)*Vm FOR ALL iz
WITH mke: derivm(PHIttrsp,ttrsp);  
! vdiff(MKE)
DO WITH mean(iz), mke(iz): PHIvdiff=nu*(U*Uz+Vm*Vz) FOR ALL iz
WITH mke: derivm(PHIvdiff,vdiff); 
! pump(MKE)
DO WITH mke(iz),mean(iz): pump=0 FOR ALL iz

! Write to binary file
! ------------------------------------
FILE out=CREATE("test/uiuj.bin")
! Re-stresses
DO WRITE BINARY TO out uiuj(iz).var FOR iz=1 TO nz-1
! Production rates
DO WRITE BINARY TO out uiuj(iz).prod FOR iz=1 TO nz-1
! Dissipation rates
DO WRITE BINARY TO out uiuj(iz).psdiss FOR iz=1 TO nz-1
! Turbulent transport
DO WRITE BINARY TO out uiuj(iz).ttrsp FOR iz=1 TO nz-1
! Viscous diffusion
DO WRITE BINARY TO out uiuj(iz).vdiff FOR iz=1 TO nz-1
! Pressure-strain
DO WRITE BINARY TO out uiuj(iz).pstrain FOR iz=1 TO nz-1
! Pressure transport
DO WRITE BINARY TO out uiuj(iz).ptrsp FOR iz=1 TO nz-1
! PHI turbulent transport
DO WRITE BINARY TO out uiuj(iz).PHIttrsp FOR iz=1 TO nz-1
! PHI viscous diffusion
DO WRITE BINARY TO out uiuj(iz).PHIvdiff FOR iz=1 TO nz-1
! PHI pressure transport
DO WRITE BINARY TO out uiuj(iz).PHIptrsp FOR iz=1 TO nz-1
CLOSE out


! Be polite and say goodbye
! ------------------------------------
WRITE " "
WRITE "Goodbye, man!"
